using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PROTECTED_StudentLayer_StronaStudent : System.Web.UI.Page
{
    private static string path = HttpContext.Current.Server.MapPath("~/App_Data/DziekanatDB.mdf");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            PobierzSemestr();
        }
    }
    protected void dane_Click(object sender, EventArgs e)
    {

        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText =
            String.Format("select nr_indeksu, imie, nazwisko, adres, data_urodzenia, pesel, data_rozpoczecia, aktualny_sem, tryb_studiow, idkierunku, mail from Studenci where Studenci.nr_indeksu = '{0}' and Studenci.haslo = '{1}'",
            (string)Session["studentLogin"], (string)Session["studentHaslo"]);
        command.CommandType = CommandType.Text;
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        GridView1.DataSource = dt;
        GridView1.DataBind();
        GridView1.Visible = true;
        connection.Close();
    }
    protected void PobierzSemestr()
    {
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString); 
        SqlCommand command = new SqlCommand();


        command.Connection = connection;
        command.CommandText = String.Format("select aktualny_sem from Studenci where Studenci.nr_indeksu = 'S1' "); // (string)Session["studentLogin"]);
        command.CommandType = CommandType.Text;

        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataSet ds = new DataSet();
        adapter.Fill(ds);

        object value = ds.Tables[0].Rows[0][0];
        value.ToString();
        int c = Convert.ToInt32(value);

        List<int> listaSemestrow = new List<int>();
        do
        {
            listaSemestrow.Add(c);
            c--;
        } while (c!=0);

        DropDownList1.DataSource = listaSemestrow;
        DropDownList1.DataBind();
        DropDownList1.Items.Insert(0, new ListItem("Wybierz Semestr", String.Empty));

        connection.Close();
    }
    protected void oceny_Click(object sender, EventArgs e)
    {

    }
    protected void ok_Click(object sender, EventArgs e)
    {
        Response.Redirect("StronaZPrzedmiotami.aspx");
    }

}