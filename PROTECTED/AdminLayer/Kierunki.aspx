﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PROTECTED/AdminLayer/MasterAdmin.master" AutoEventWireup="true" CodeFile="Kierunki.aspx.cs" Inherits="PROTECTED_AdminLayer_Kierunki" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Kierunki</h1>
    <p>
        ID Kierunku:<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
        <asp:Button ID="Button6" runat="server" OnClick="Button1_Click" Text="Dodaj Przedmiot" />

        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="Szukaj Kierunku" OnClick="Button1_Click" />
        <asp:Button ID="Button2" runat="server" Text="Dodaj Kierunek" OnClick="Button2_Click" />
    </p>
    <asp:GridView ID="GridView1" runat="server" CellPadding="4" DataKeyNames="Id" EnableModelValidation="True" ForeColor="#333333" GridLines="None" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="False">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" />
            <asp:BoundField DataField="nazwa" HeaderText="Nazwa kierunku" />
            <asp:CommandField DeleteText="Usuń" ShowDeleteButton="True" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
    </asp:GridView>
    
    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
   <p> <asp:Button ID="Button5" runat="server" Text="Pokaż wszystkie" OnClick="Button5_Click" /></p>
    <br />
    <asp:Label ID="Label2" runat="server" Text="ID Kierunku:"></asp:Label>
    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
    <asp:Button ID="Button3" runat="server" Text="Szukaj Studentów" OnClick="Button3_Click" />
    <asp:Button ID="Button4" runat="server" Text="Szukaj Przedmiotów" OnClick="Button4_Click" />
    <asp:Button ID="btnNewEntry" runat="Server" CssClass="button" Text="Dodaj przedmiot do kierunku" OnClick="btnNewEntry_Click" />
    <asp:GridView ID="GridView2" runat="server" EnableModelValidation="True" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical">
        <AlternatingRowStyle BackColor="White" />
        <FooterStyle BackColor="#CCCC99" />
        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
        <RowStyle BackColor="#F7F7DE" />
        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
    </asp:GridView>
</asp:Content>

